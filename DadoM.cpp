#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <graphics.h>

#define FACCE 6
#define VERTICI 4
#define PUNTI 7

struct punto2d
{
       double x, y;
};

struct punto3d
{
       double x, y, z, riservato;
};

struct punto_sferico
{
       double ro, fi, teta;
};

typedef struct punto2d PUNTO2D;
typedef struct punto3d PUNTO3D;
typedef struct punto_sferico PUNTOSFERICO;

struct poligono3d
{
       PUNTO3D vertice[VERTICI];
       PUNTO3D dot[PUNTI][VERTICI];
       int visibile, dot_visibile[PUNTI];       
};

struct poligono2d
{
       PUNTO2D punto[VERTICI];
       PUNTO2D dot[PUNTI][VERTICI];
       int visibile, dot_visibile[PUNTI];       
};

typedef struct poligono3d POLIGONO3D;
typedef struct poligono2d POLIGONO2D;

void conversionePuntoDiVista( PUNTOSFERICO *, PUNTO3D * );
void conversionePuntoSfericoaPuntoCartesiano( PUNTO3D *, PUNTOSFERICO * );   
void trasformazioneFacceCordinateDiVista( PUNTOSFERICO *, POLIGONO3D *, POLIGONO3D *, int ); 
void trasformazioneNelleCordinateDiVista( PUNTOSFERICO *, PUNTO3D *, PUNTO3D *, int );
void trasformazioniFacceCordinateDueD( double, POLIGONO2D *, POLIGONO3D *, int );  
void trasformazioneNelleCordinateDueD( double, PUNTO2D *, PUNTO3D *, int );
void disegnaFaccia( PUNTO2D *, int );
void disegnaSolido( POLIGONO2D *, int );
double diemnsioneMaxOggetto( POLIGONO3D * );
double maxSchermo();
double distanzaCamera( PUNTOSFERICO *, POLIGONO3D * );
int arrotonda( double );
void DisegnaCubo( POLIGONO3D *, PUNTOSFERICO * );
int CalcoloFacciaNascota( PUNTO3D *, PUNTO3D *, int );
int VerticePiuSinistra( PUNTO3D *, int );
void MovimentoMouseCubo( int, int ); 


POLIGONO3D cubo[FACCE];
PUNTO3D punto_vista;
PUNTOSFERICO puntoVistaS;

int main()
{
    float addVal;
    // FACCIA 0
    // PUNTO A
    cubo[0].vertice[1].x = 1;
    cubo[0].vertice[1].y = -1;
    cubo[0].vertice[1].z = -1;
    cubo[0].vertice[1].riservato = 1;
   
    // PUNTO B
    cubo[0].vertice[0].x = 1;
    cubo[0].vertice[0].y = 1;
    cubo[0].vertice[0].z = -1;
    cubo[0].vertice[0].riservato = 1;
    
    // PUNTO C
    cubo[0].vertice[3].x = -1;
    cubo[0].vertice[3].y = 1;
    cubo[0].vertice[3].z = -1;
    cubo[0].vertice[3].riservato = 1;
    
    // PUNTO D
    cubo[0].vertice[2].x = -1;
    cubo[0].vertice[2].y = -1;
    cubo[0].vertice[2].z = -1;
    cubo[0].vertice[2].riservato = 1;
    
    cubo[0].visibile = 1;
    
         // SCORE PER FACCIA 0
         
         // PUNTO 0
         cubo[0].dot[0][0].x = -0.8;
         cubo[0].dot[0][0].y = -0.8;
         cubo[0].dot[0][0].z = -1;
         cubo[0].dot[0][0].riservato = 1;
         
         cubo[0].dot[0][1].x = -0.5;
         cubo[0].dot[0][1].y = -0.8;
         cubo[0].dot[0][1].z = -1;
         cubo[0].dot[0][1].riservato = 1;
         
         cubo[0].dot[0][2].x = -0.5;
         cubo[0].dot[0][2].y = -0.5;
         cubo[0].dot[0][2].z = -1;
         cubo[0].dot[0][2].riservato = 1;
         
         cubo[0].dot[0][3].x = -0.8;
         cubo[0].dot[0][3].y = -0.5;
         cubo[0].dot[0][3].z = -1;
         cubo[0].dot[0][3].riservato = 1;
         
         cubo[0].dot_visibile[0] = 0;
         
         // PUNTO 1
         cubo[0].dot[1][0].x = 0.5;
         cubo[0].dot[1][0].y = -0.8;
         cubo[0].dot[1][0].z = -1;
         cubo[0].dot[1][0].riservato = 1;
         
         cubo[0].dot[1][1].x = 0.8;
         cubo[0].dot[1][1].y = -0.8;
         cubo[0].dot[1][1].z = -1;
         cubo[0].dot[1][1].riservato = 1;
         
         cubo[0].dot[1][2].x = 0.8;
         cubo[0].dot[1][2].y = -0.5;
         cubo[0].dot[1][2].z = -1;
         cubo[0].dot[1][2].riservato = 1;
         
         cubo[0].dot[1][3].x = 0.5;
         cubo[0].dot[1][3].y = -0.5;
         cubo[0].dot[1][3].z = -1;
         cubo[0].dot[1][3].riservato = 1;
         
         cubo[0].dot_visibile[1] = 0;
         
         // PUNTO 2
         cubo[0].dot[2][0].x = -0.8;
         cubo[0].dot[2][0].y = -0.15;
         cubo[0].dot[2][0].z = -1;
         cubo[0].dot[2][0].riservato = 1;
         
         cubo[0].dot[2][1].x = -0.5;
         cubo[0].dot[2][1].y = -0.15;
         cubo[0].dot[2][1].z = -1;
         cubo[0].dot[2][1].riservato = 1;
         
         cubo[0].dot[2][2].x = -0.5;
         cubo[0].dot[2][2].y = 0.15;
         cubo[0].dot[2][2].z = -1;
         cubo[0].dot[2][2].riservato = 1;
         
         cubo[0].dot[2][3].x = -0.8;
         cubo[0].dot[2][3].y = 0.15;
         cubo[0].dot[2][3].z = -1;
         cubo[0].dot[2][3].riservato = 1;
         
         cubo[0].dot_visibile[2] = 0;
         
         // PUNTO 3
         cubo[0].dot[3][0].x = -0.15;
         cubo[0].dot[3][0].y = -0.15;
         cubo[0].dot[3][0].z = -1;
         cubo[0].dot[3][0].riservato = 1;
         
         cubo[0].dot[3][1].x = 0.15;
         cubo[0].dot[3][1].y = -0.15;
         cubo[0].dot[3][1].z = -1;
         cubo[0].dot[3][1].riservato = 1;
         
         cubo[0].dot[3][2].x = 0.15;
         cubo[0].dot[3][2].y = 0.15;
         cubo[0].dot[3][2].z = -1;
         cubo[0].dot[3][2].riservato = 1;
         
         cubo[0].dot[3][3].x = -0.15;
         cubo[0].dot[3][3].y = 0.15;
         cubo[0].dot[3][3].z = -1;
         cubo[0].dot[3][3].riservato = 1;
         
         cubo[0].dot_visibile[3] = 1;
         
         // PUNTO 4
         cubo[0].dot[4][0].x = 0.5;
         cubo[0].dot[4][0].y = -0.15;
         cubo[0].dot[4][0].z = -1;
         cubo[0].dot[4][0].riservato = 1;
         
         cubo[0].dot[4][1].x = 0.8;
         cubo[0].dot[4][1].y = -0.15;
         cubo[0].dot[4][1].z = -1;
         cubo[0].dot[4][1].riservato = 1;
         
         cubo[0].dot[4][2].x = 0.8;
         cubo[0].dot[4][2].y = 0.15;
         cubo[0].dot[4][2].z = -1;
         cubo[0].dot[4][2].riservato = 1;
         
         cubo[0].dot[4][3].x = 0.5;
         cubo[0].dot[4][3].y = 0.15;
         cubo[0].dot[4][3].z = -1;
         cubo[0].dot[4][3].riservato = 1;
         
         cubo[0].dot_visibile[4] = 0;
         
         // PUNTO 5
         cubo[0].dot[5][0].x = -0.8;
         cubo[0].dot[5][0].y = 0.5;
         cubo[0].dot[5][0].z = -1;
         cubo[0].dot[5][0].riservato = 1;
         
         cubo[0].dot[5][1].x = -0.5;
         cubo[0].dot[5][1].y = 0.5;
         cubo[0].dot[5][1].z = -1;
         cubo[0].dot[5][1].riservato = 1;
         
         cubo[0].dot[5][2].x = -0.5;
         cubo[0].dot[5][2].y = 0.8;
         cubo[0].dot[5][2].z = -1;
         cubo[0].dot[5][2].riservato = 1;
         
         cubo[0].dot[5][3].x = -0.8;
         cubo[0].dot[5][3].y = 0.8;
         cubo[0].dot[5][3].z = -1;
         cubo[0].dot[5][3].riservato = 1;
         
         cubo[0].dot_visibile[5] = 0;
         
         // PUNTO 6
         cubo[0].dot[6][0].x = 0.5;
         cubo[0].dot[6][0].y = 0.5;
         cubo[0].dot[6][0].z = -1;
         cubo[0].dot[6][0].riservato = 1;
         
         cubo[0].dot[6][1].x = 0.8;
         cubo[0].dot[6][1].y = 0.5;
         cubo[0].dot[6][1].z = -1;
         cubo[0].dot[6][1].riservato = 1;
         
         cubo[0].dot[6][2].x = 0.8;
         cubo[0].dot[6][2].y = 0.8;
         cubo[0].dot[6][2].z = -1;
         cubo[0].dot[6][2].riservato = 1;
         
         cubo[0].dot[6][3].x = 0.5;
         cubo[0].dot[6][3].y = 0.8;
         cubo[0].dot[6][3].z = -1;
         cubo[0].dot[6][3].riservato = 1;
         
         cubo[0].dot_visibile[6] = 0;
    
    // FACCIA 1
    // PUNTO A
    cubo[1].vertice[0].x = 1;
    cubo[1].vertice[0].y = -1;
    cubo[1].vertice[0].z = -1;
    cubo[1].vertice[0].riservato = 1;
   
    // PUNTO B
    cubo[1].vertice[1].x = 1;
    cubo[1].vertice[1].y = 1;
    cubo[1].vertice[1].z = -1;
    cubo[1].vertice[1].riservato = 1;
    
    // PUNTO F
    cubo[1].vertice[2].x = 1;
    cubo[1].vertice[2].y = 1;
    cubo[1].vertice[2].z = 1;
    cubo[1].vertice[2].riservato = 1;
   
    // PUNTO E
    cubo[1].vertice[3].x = 1;
    cubo[1].vertice[3].y = -1;
    cubo[1].vertice[3].z = 1;
    cubo[1].vertice[3].riservato = 1;
    
    cubo[1].visibile = 1;
    
    // SCORE PER FACCIA 1
         
         // PUNTO 0
         cubo[1].dot[0][0].y = -0.8;
         cubo[1].dot[0][0].z = -0.8;
         cubo[1].dot[0][0].x = 1;
         cubo[1].dot[0][0].riservato = 1;
         
         cubo[1].dot[0][1].y = -0.5;
         cubo[1].dot[0][1].z = -0.8;
         cubo[1].dot[0][1].x = 1;
         cubo[1].dot[0][1].riservato = 1;
         
         cubo[1].dot[0][2].y = -0.5;
         cubo[1].dot[0][2].z = -0.5;
         cubo[1].dot[0][2].x = 1;
         cubo[1].dot[0][2].riservato = 1;
         
         cubo[1].dot[0][3].y = -0.8;
         cubo[1].dot[0][3].z = -0.5;
         cubo[1].dot[0][3].x = 1;
         cubo[1].dot[0][3].riservato = 1;
         
         cubo[1].dot_visibile[0] = 1;
         
         // PUNTO 1
         cubo[1].dot[1][0].y = 0.5;
         cubo[1].dot[1][0].z = -0.8;
         cubo[1].dot[1][0].x = 1;
         cubo[1].dot[1][0].riservato = 1;
         
         cubo[1].dot[1][1].y = 0.8;
         cubo[1].dot[1][1].z = -0.8;
         cubo[1].dot[1][1].x = 1;
         cubo[1].dot[1][1].riservato = 1;
         
         cubo[1].dot[1][2].y = 0.8;
         cubo[1].dot[1][2].z = -0.5;
         cubo[1].dot[1][2].x = 1;
         cubo[1].dot[1][2].riservato = 1;
         
         cubo[1].dot[1][3].y = 0.5;
         cubo[1].dot[1][3].z = -0.5;
         cubo[1].dot[1][3].x = 1;
         cubo[1].dot[1][3].riservato = 1;
         
         cubo[1].dot_visibile[1] = 1;
         
         // PUNTO 2
         cubo[1].dot[2][0].y = -0.8;
         cubo[1].dot[2][0].z = -0.15;
         cubo[1].dot[2][0].x = 1;
         cubo[1].dot[2][0].riservato = 1;
         
         cubo[1].dot[2][1].y = -0.5;
         cubo[1].dot[2][1].z = -0.15;
         cubo[1].dot[2][1].x = 1;
         cubo[1].dot[2][1].riservato = 1;
         
         cubo[1].dot[2][2].y = -0.5;
         cubo[1].dot[2][2].z = 0.15;
         cubo[1].dot[2][2].x = 1;
         cubo[1].dot[2][2].riservato = 1;
         
         cubo[1].dot[2][3].y = -0.8;
         cubo[1].dot[2][3].z = 0.15;
         cubo[1].dot[2][3].x = 1;
         cubo[1].dot[2][3].riservato = 1;
         
         cubo[1].dot_visibile[2] = 0;
         
         // PUNTO 3
         cubo[1].dot[3][0].y = -0.15;
         cubo[1].dot[3][0].z = -0.15;
         cubo[1].dot[3][0].x = 1;
         cubo[1].dot[3][0].riservato = 1;
         
         cubo[1].dot[3][1].y = 0.15;
         cubo[1].dot[3][1].z = -0.15;
         cubo[1].dot[3][1].x = 1;
         cubo[1].dot[3][1].riservato = 1;
         
         cubo[1].dot[3][2].y = 0.15;
         cubo[1].dot[3][2].z = 0.15;
         cubo[1].dot[3][2].x = 1;
         cubo[1].dot[3][2].riservato = 1;
         
         cubo[1].dot[3][3].y = -0.15;
         cubo[1].dot[3][3].z = 0.15;
         cubo[1].dot[3][3].x = 1;
         cubo[1].dot[3][3].riservato = 1;
         
         cubo[1].dot_visibile[3] = 1;
         
         // PUNTO 4
         cubo[1].dot[4][0].y = 0.5;
         cubo[1].dot[4][0].z = -0.15;
         cubo[1].dot[4][0].x = 1;
         cubo[1].dot[4][0].riservato = 1;
         
         cubo[1].dot[4][1].y = 0.8;
         cubo[1].dot[4][1].z = -0.15;
         cubo[1].dot[4][1].x = 1;
         cubo[1].dot[4][1].riservato = 1;
         
         cubo[1].dot[4][2].y = 0.8;
         cubo[1].dot[4][2].z = 0.15;
         cubo[1].dot[4][2].x = 1;
         cubo[1].dot[4][2].riservato = 1;
         
         cubo[1].dot[4][3].y = 0.5;
         cubo[1].dot[4][3].z = 0.15;
         cubo[1].dot[4][3].x = 1;
         cubo[1].dot[4][3].riservato = 1;
         
         cubo[1].dot_visibile[4] = 0;
         
         // PUNTO 5
         cubo[1].dot[5][0].y = -0.8;
         cubo[1].dot[5][0].z = 0.5;
         cubo[1].dot[5][0].x = 1;
         cubo[1].dot[5][0].riservato = 1;
    
         cubo[1].dot[5][1].y = -0.5;
         cubo[1].dot[5][1].z = 0.5;
         cubo[1].dot[5][1].x = 1;
         cubo[1].dot[5][1].riservato = 1;
         
         cubo[1].dot[5][2].y = -0.5;
         cubo[1].dot[5][2].z = 0.8;
         cubo[1].dot[5][2].x = 1;
         cubo[1].dot[5][2].riservato = 1;
         
         cubo[1].dot[5][3].y = -0.8;
         cubo[1].dot[5][3].z = 0.8;
         cubo[1].dot[5][3].x = 1;
         cubo[1].dot[5][3].riservato = 1;
         
         cubo[1].dot_visibile[5] = 1;
         
         // PUNTO 6
         cubo[1].dot[6][0].y = 0.5;
         cubo[1].dot[6][0].z = 0.5;
         cubo[1].dot[6][0].x = 1;
         cubo[1].dot[6][0].riservato = 1;
         
         cubo[1].dot[6][1].y = 0.8;
         cubo[1].dot[6][1].z = 0.5;
         cubo[1].dot[6][1].x = 1;
         cubo[1].dot[6][1].riservato = 1;
         
         cubo[1].dot[6][2].y = 0.8;
         cubo[1].dot[6][2].z = 0.8;
         cubo[1].dot[6][2].x = 1;
         cubo[1].dot[6][2].riservato = 1;
         
         cubo[1].dot[6][3].y = 0.5;
         cubo[1].dot[6][3].z = 0.8;
         cubo[1].dot[6][3].x = 1;
         cubo[1].dot[6][3].riservato = 1;
         
         cubo[1].dot_visibile[6] = 1;
    
    // FACCIA 2
    // PUNTO B
    cubo[2].vertice[0].x = 1;
    cubo[2].vertice[0].y = 1;
    cubo[2].vertice[0].z = -1;
    cubo[2].vertice[0].riservato = 1;
    
    // PUNTO C
    cubo[2].vertice[1].x = -1;
    cubo[2].vertice[1].y = 1;
    cubo[2].vertice[1].z = -1;
    cubo[2].vertice[1].riservato = 1;
  
    // PUNTO G
    cubo[2].vertice[2].x = -1;
    cubo[2].vertice[2].y = 1;
    cubo[2].vertice[2].z = 1;
    cubo[2].vertice[2].riservato = 1;
    
    // PUNTO F
    cubo[2].vertice[3].x = 1;
    cubo[2].vertice[3].y = 1;
    cubo[2].vertice[3].z = 1;
    cubo[2].vertice[3].riservato = 1;
    
    cubo[2].visibile = 1;
    
    // SCORE PER FACCIA 2
         
         // PUNTO 0
         cubo[2].dot[0][0].x = -0.8;
         cubo[2].dot[0][0].z = -0.8;
         cubo[2].dot[0][0].y = 1;
         cubo[2].dot[0][0].riservato = 1;
         
         cubo[2].dot[0][1].x = -0.5;
         cubo[2].dot[0][1].z = -0.8;
         cubo[2].dot[0][1].y = 1;
         cubo[2].dot[0][1].riservato = 1;
         
         cubo[2].dot[0][2].x = -0.5;
         cubo[2].dot[0][2].z = -0.5;
         cubo[2].dot[0][2].y = 1;
         cubo[2].dot[0][2].riservato = 1;
         
         cubo[2].dot[0][3].x = -0.8;
         cubo[2].dot[0][3].z = -0.5;
         cubo[2].dot[0][3].y = 1;
         cubo[2].dot[0][3].riservato = 1;
         
         cubo[2].dot_visibile[0] = 1;
         
         // PUNTO 1
         cubo[2].dot[1][0].x = 0.5;
         cubo[2].dot[1][0].z = -0.8;
         cubo[2].dot[1][0].y = 1;
         cubo[2].dot[1][0].riservato = 1;
         
         cubo[2].dot[1][1].x = 0.8;
         cubo[2].dot[1][1].z = -0.8;
         cubo[2].dot[1][1].y = 1;
         cubo[2].dot[1][1].riservato = 1;
         
         cubo[2].dot[1][2].x = 0.8;
         cubo[2].dot[1][2].z = -0.5;
         cubo[2].dot[1][2].y = 1;
         cubo[2].dot[1][2].riservato = 1;
         
         cubo[2].dot[1][3].x = 0.5;
         cubo[2].dot[1][3].z = -0.5;
         cubo[2].dot[1][3].y = 1;
         cubo[2].dot[1][3].riservato = 1;
         
         cubo[2].dot_visibile[1] = 1;
         
         // PUNTO 2
         cubo[2].dot[2][0].x = -0.8;
         cubo[2].dot[2][0].z = -0.15;
         cubo[2].dot[2][0].y = 1;
         cubo[2].dot[2][0].riservato = 1;
         
         cubo[2].dot[2][1].x = -0.5;
         cubo[2].dot[2][1].z = -0.15;
         cubo[2].dot[2][1].y = 1;
         cubo[2].dot[2][1].riservato = 1;
         
         cubo[2].dot[2][2].x = -0.5;
         cubo[2].dot[2][2].z = 0.15;
         cubo[2].dot[2][2].y = 1;
         cubo[2].dot[2][2].riservato = 1;
         
         cubo[2].dot[2][3].x = -0.8;
         cubo[2].dot[2][3].z = 0.15;
         cubo[2].dot[2][3].y = 1;
         cubo[2].dot[2][3].riservato = 1;
         
         cubo[2].dot_visibile[2] = 0;
         
         // PUNTO 3
         cubo[2].dot[3][0].x = -0.15;
         cubo[2].dot[3][0].z = -0.15;
         cubo[2].dot[3][0].y = 1;
         cubo[2].dot[3][0].riservato = 1;
         
         cubo[2].dot[3][1].x = 0.15;
         cubo[2].dot[3][1].z = -0.15;
         cubo[2].dot[3][1].y = 1;
         cubo[2].dot[3][1].riservato = 1;
         
         cubo[2].dot[3][2].x = 0.15;
         cubo[2].dot[3][2].z = 0.15;
         cubo[2].dot[3][2].y = 1;
         cubo[2].dot[3][2].riservato = 1;
         
         cubo[2].dot[3][3].x = -0.15;
         cubo[2].dot[3][3].z = 0.15;
         cubo[2].dot[3][3].y = 1;
         cubo[2].dot[3][3].riservato = 1;
         
         cubo[2].dot_visibile[3] = 0;
         
         // PUNTO 4
         cubo[2].dot[4][0].x = 0.5;
         cubo[2].dot[4][0].z = -0.15;
         cubo[2].dot[4][0].y = 1;
         cubo[2].dot[4][0].riservato = 1;
         
         cubo[2].dot[4][1].x = 0.8;
         cubo[2].dot[4][1].z = -0.15;
         cubo[2].dot[4][1].y = 1;
         cubo[2].dot[4][1].riservato = 1;
         
         cubo[2].dot[4][2].x = 0.8;
         cubo[2].dot[4][2].z = 0.15;
         cubo[2].dot[4][2].y = 1;
         cubo[2].dot[4][2].riservato = 1;
         
         cubo[2].dot[4][3].x = 0.5;
         cubo[2].dot[4][3].z = 0.15;
         cubo[2].dot[4][3].y = 1;
         cubo[2].dot[4][3].riservato = 1;
         
         cubo[2].dot_visibile[4] = 0;
         
         // PUNTO 5
         cubo[2].dot[5][0].x = -0.8;
         cubo[2].dot[5][0].z = 0.5;
         cubo[2].dot[5][0].y = 1;
         cubo[2].dot[5][0].riservato = 1;
         
         cubo[2].dot[5][1].x = -0.5;
         cubo[2].dot[5][1].z = 0.5;
         cubo[2].dot[5][1].y = 1;
         cubo[2].dot[5][1].riservato = 1;
         
         cubo[2].dot[5][2].x = -0.5;
         cubo[2].dot[5][2].z = 0.8;
         cubo[2].dot[5][2].y = 1;
         cubo[2].dot[5][2].riservato = 1;
         
         cubo[2].dot[5][3].x = -0.8;
         cubo[2].dot[5][3].z = 0.8;
         cubo[2].dot[5][3].y = 1;
         cubo[2].dot[5][3].riservato = 1;
         
         cubo[2].dot_visibile[5] = 1;
         
         // PUNTO 6
         cubo[2].dot[6][0].x = 0.5;
         cubo[2].dot[6][0].z = 0.5;
         cubo[2].dot[6][0].y = 1;
         cubo[2].dot[6][0].riservato = 1;
         
         cubo[2].dot[6][1].x = 0.8;
         cubo[2].dot[6][1].z = 0.5;
         cubo[2].dot[6][1].y = 1;
         cubo[2].dot[6][1].riservato = 1;
         
         cubo[2].dot[6][2].x = 0.8;
         cubo[2].dot[6][2].z = 0.8;
         cubo[2].dot[6][2].y = 1;
         cubo[2].dot[6][2].riservato = 1;
         
         cubo[2].dot[6][3].x = 0.5;
         cubo[2].dot[6][3].z = 0.8;
         cubo[2].dot[6][3].y = 1;
         cubo[2].dot[6][3].riservato = 1;
         
         cubo[2].dot_visibile[6] = 1;
    
    // FACCIA 3
    // PUNTO D   
    cubo[3].vertice[1].x = -1;
    cubo[3].vertice[1].y = -1;
    cubo[3].vertice[1].z = -1;
    cubo[3].vertice[1].riservato = 1;  
    
    // PUNTO C
    cubo[3].vertice[0].x = -1;
    cubo[3].vertice[0].y = 1;
    cubo[3].vertice[0].z = -1;
    cubo[3].vertice[0].riservato = 1;
    
    // PUNTO G
    cubo[3].vertice[3].x = -1;
    cubo[3].vertice[3].y = 1;
    cubo[3].vertice[3].z = 1;
    cubo[3].vertice[3].riservato = 1;
    
    // PUNTO H
    cubo[3].vertice[2].x = -1;
    cubo[3].vertice[2].y = -1;
    cubo[3].vertice[2].z = 1;
    cubo[3].vertice[2].riservato = 1;
    
    cubo[3].visibile = 1;
    
    // SCORE PER FACCIA 3
         
         // PUNTO 0
         cubo[3].dot[0][0].y = -0.8;
         cubo[3].dot[0][0].z = -0.8;
         cubo[3].dot[0][0].x = -1;
         cubo[3].dot[0][0].riservato = 1;
         
         cubo[3].dot[0][1].y = -0.5;
         cubo[3].dot[0][1].z = -0.8;
         cubo[3].dot[0][1].x = -1;
         cubo[3].dot[0][1].riservato = 1;
         
         cubo[3].dot[0][2].y = -0.5;
         cubo[3].dot[0][2].z = -0.5;
         cubo[3].dot[0][2].x = -1;
         cubo[3].dot[0][2].riservato = 1;
         
         cubo[3].dot[0][3].y = -0.8;
         cubo[3].dot[0][3].z = -0.5;
         cubo[3].dot[0][3].x = -1;
         cubo[3].dot[0][3].riservato = 1;
         
         cubo[3].dot_visibile[0] = 1;
         
         // PUNTO 1
         cubo[3].dot[1][0].y = 0.5;
         cubo[3].dot[1][0].z = -0.8;
         cubo[3].dot[1][0].x = -1;
         cubo[3].dot[1][0].riservato = 1;
         
         cubo[3].dot[1][1].y = 0.8;
         cubo[3].dot[1][1].z = -0.8;
         cubo[3].dot[1][1].x = -1;
         cubo[3].dot[1][1].riservato = 1;
         
         cubo[3].dot[1][2].y = 0.8;
         cubo[3].dot[1][2].z = -0.5;
         cubo[3].dot[1][2].x = -1;
         cubo[3].dot[1][2].riservato = 1;
         
         cubo[3].dot[1][3].y = 0.5;
         cubo[3].dot[1][3].z = -0.5;
         cubo[3].dot[1][3].x = -1;
         cubo[3].dot[1][3].riservato = 1;
         
         cubo[3].dot_visibile[1] = 0;
         
         // PUNTO 2
         cubo[3].dot[2][0].y = -0.8;
         cubo[3].dot[2][0].z = -0.15;
         cubo[3].dot[2][0].x = -1;
         cubo[3].dot[2][0].riservato = 1;
         
         cubo[3].dot[2][1].y = -0.5;
         cubo[3].dot[2][1].z = -0.15;
         cubo[3].dot[2][1].x = -1;
         cubo[3].dot[2][1].riservato = 1;
         
         cubo[3].dot[2][2].y = -0.5;
         cubo[3].dot[2][2].z = 0.15;
         cubo[3].dot[2][2].x = -1;
         cubo[3].dot[2][2].riservato = 1;
         
         cubo[3].dot[2][3].y = -0.8;
         cubo[3].dot[2][3].z = 0.15;
         cubo[3].dot[2][3].x = -1;
         cubo[3].dot[2][3].riservato = 1;
         
         cubo[3].dot_visibile[2] = 0;
         
         // PUNTO 3
         cubo[3].dot[3][0].y = -0.15;
         cubo[3].dot[3][0].z = -0.15;
         cubo[3].dot[3][0].x = -1;
         cubo[3].dot[3][0].riservato = 1;
         
         cubo[3].dot[3][1].y = 0.15;
         cubo[3].dot[3][1].z = -0.15;
         cubo[3].dot[3][1].x = -1;
         cubo[3].dot[3][1].riservato = 1;
         
         cubo[3].dot[3][2].y = 0.15;
         cubo[3].dot[3][2].z = 0.15;
         cubo[3].dot[3][2].x = -1;
         cubo[3].dot[3][2].riservato = 1;
         
         cubo[3].dot[3][3].y = -0.15;
         cubo[3].dot[3][3].z = 0.15;
         cubo[3].dot[3][3].x = -1;
         cubo[3].dot[3][3].riservato = 1;
         
         cubo[3].dot_visibile[3] = 0;
         
         // PUNTO 4
         cubo[3].dot[4][0].y = 0.5;
         cubo[3].dot[4][0].z = -0.15;
         cubo[3].dot[4][0].x = -1;
         cubo[3].dot[4][0].riservato = 1;
         
         cubo[3].dot[4][1].y = 0.8;
         cubo[3].dot[4][1].z = -0.15;
         cubo[3].dot[4][1].x = -1;
         cubo[3].dot[4][1].riservato = 1;
         
         cubo[3].dot[4][2].y = 0.8;
         cubo[3].dot[4][2].z = 0.15;
         cubo[3].dot[4][2].x = -1;
         cubo[3].dot[4][2].riservato = 1;
         
         cubo[3].dot[4][3].y = 0.5;
         cubo[3].dot[4][3].z = 0.15;
         cubo[3].dot[4][3].x = -1;
         cubo[3].dot[4][3].riservato = 1;
         
         cubo[3].dot_visibile[4] = 0;
         
         // PUNTO 5
         cubo[3].dot[5][0].y = -0.8;
         cubo[3].dot[5][0].z = 0.5;
         cubo[3].dot[5][0].x = -1;
         cubo[3].dot[5][0].riservato = 1;
    
         cubo[3].dot[5][1].y = -0.5;
         cubo[3].dot[5][1].z = 0.5;
         cubo[3].dot[5][1].x = -1;
         cubo[3].dot[5][1].riservato = 1;
         
         cubo[3].dot[5][2].y = -0.5;
         cubo[3].dot[5][2].z = 0.8;
         cubo[3].dot[5][2].x = -1;
         cubo[3].dot[5][2].riservato = 1;
         
         cubo[3].dot[5][3].y = -0.8;
         cubo[3].dot[5][3].z = 0.8;
         cubo[3].dot[5][3].x = -1;
         cubo[3].dot[5][3].riservato = 1;
         
         cubo[3].dot_visibile[5] = 0;
         
         // PUNTO 6
         cubo[3].dot[6][0].y = 0.5;
         cubo[3].dot[6][0].z = 0.5;
         cubo[3].dot[6][0].x = -1;
         cubo[3].dot[6][0].riservato = 1;
    
         cubo[3].dot[6][1].y = 0.8;
         cubo[3].dot[6][1].z = 0.5;
         cubo[3].dot[6][1].x = -1;
         cubo[3].dot[6][1].riservato = 1;
    
         cubo[3].dot[6][2].y = 0.8;
         cubo[3].dot[6][2].z = 0.8;
         cubo[3].dot[6][2].x = -1;
         cubo[3].dot[6][2].riservato = 1;
         
         cubo[3].dot[6][3].y = 0.5;
         cubo[3].dot[6][3].z = 0.8;
         cubo[3].dot[6][3].x = -1;
         cubo[3].dot[6][3].riservato = 1;
         
         cubo[3].dot_visibile[6] = 1;
    
    // FACCIA 4
    // PUNTO A
    cubo[4].vertice[1].x = 1;
    cubo[4].vertice[1].y = -1;
    cubo[4].vertice[1].z = -1;
    cubo[4].vertice[1].riservato = 1;
    
    // PUNTO D   
    cubo[4].vertice[0].x = -1;
    cubo[4].vertice[0].y = -1;
    cubo[4].vertice[0].z = -1;
    cubo[4].vertice[0].riservato = 1; 

    // PUNTO H
    cubo[4].vertice[3].x = -1;
    cubo[4].vertice[3].y = -1;
    cubo[4].vertice[3].z = 1;
    cubo[4].vertice[3].riservato = 1;
    
    // PUNTO E
    cubo[4].vertice[2].x = 1;
    cubo[4].vertice[2].y = -1;
    cubo[4].vertice[2].z = 1;
    cubo[4].vertice[2].riservato = 1; 
    
    cubo[4].visibile = 1;
    
    // SCORE PER FACCIA 4
         
         // PUNTO 0
         cubo[4].dot[0][0].x = -0.8;
         cubo[4].dot[0][0].z = -0.8;
         cubo[4].dot[0][0].y = -1;
         cubo[4].dot[0][0].riservato = 1;
         
         cubo[4].dot[0][1].x = -0.5;
         cubo[4].dot[0][1].z = -0.8;
         cubo[4].dot[0][1].y = -1;
         cubo[4].dot[0][1].riservato = 1;
         
         cubo[4].dot[0][2].x = -0.5;
         cubo[4].dot[0][2].z = -0.5;
         cubo[4].dot[0][2].y = -1;
         cubo[4].dot[0][2].riservato = 1;
         
         cubo[4].dot[0][3].x = -0.8;
         cubo[4].dot[0][3].z = -0.5;
         cubo[4].dot[0][3].y = -1;
         cubo[4].dot[0][3].riservato = 1;
         
         cubo[4].dot_visibile[0] = 1;
         
         // PUNTO 1
         cubo[4].dot[1][0].x = 0.5;
         cubo[4].dot[1][0].z = -0.8;
         cubo[4].dot[1][0].y = -1;
         cubo[4].dot[1][0].riservato = 1;
         
         cubo[4].dot[1][1].x = 0.8;
         cubo[4].dot[1][1].z = -0.8;
         cubo[4].dot[1][1].y = -1;
         cubo[4].dot[1][1].riservato = 1;
         
         cubo[4].dot[1][2].x = 0.8;
         cubo[4].dot[1][2].z = -0.5;
         cubo[4].dot[1][2].y = -1;
         cubo[4].dot[1][2].riservato = 1;
         
         cubo[4].dot[1][3].x = 0.5;
         cubo[4].dot[1][3].z = -0.5;
         cubo[4].dot[1][3].y = -1;
         cubo[4].dot[1][3].riservato = 1;
         
         cubo[4].dot_visibile[1] = 0;
         
         // PUNTO 2
         cubo[4].dot[2][0].x = -0.8;
         cubo[4].dot[2][0].z = -0.15;
         cubo[4].dot[2][0].y = -1;
         cubo[4].dot[2][0].riservato = 1;
         
         cubo[4].dot[2][1].x = -0.5;
         cubo[4].dot[2][1].z = -0.15;
         cubo[4].dot[2][1].y = -1;
         cubo[4].dot[2][1].riservato = 1;
         
         cubo[4].dot[2][2].x = -0.5;
         cubo[4].dot[2][2].z = 0.15;
         cubo[4].dot[2][2].y = -1;
         cubo[4].dot[2][2].riservato = 1;
         
         cubo[4].dot[2][3].x = -0.8;
         cubo[4].dot[2][3].z = 0.15;
         cubo[4].dot[2][3].y = -1;
         cubo[4].dot[2][3].riservato = 1;
         
         cubo[4].dot_visibile[2] = 0;
         
         // PUNTO 3
         cubo[4].dot[3][0].x = -0.15;
         cubo[4].dot[3][0].z = -0.15;
         cubo[4].dot[3][0].y = -1;
         cubo[4].dot[3][0].riservato = 1;
         
         cubo[4].dot[3][1].x = 0.15;
         cubo[4].dot[3][1].z = -0.15;
         cubo[4].dot[3][1].y = -1;
         cubo[4].dot[3][1].riservato = 1;
         
         cubo[4].dot[3][2].x = 0.15;
         cubo[4].dot[3][2].z = 0.15;
         cubo[4].dot[3][2].y = -1;
         cubo[4].dot[3][2].riservato = 1;
         
         cubo[4].dot[3][3].x = -0.15;
         cubo[4].dot[3][3].z = 0.15;
         cubo[4].dot[3][3].y = -1;
         cubo[4].dot[3][3].riservato = 1;
         
         cubo[4].dot_visibile[3] = 1;
         
         // PUNTO 4
         cubo[4].dot[4][0].x = 0.5;
         cubo[4].dot[4][0].z = -0.15;
         cubo[4].dot[4][0].y = -1;
         cubo[4].dot[4][0].riservato = 1;
         
         cubo[4].dot[4][1].x = 0.8;
         cubo[4].dot[4][1].z = -0.15;
         cubo[4].dot[4][1].y = -1;
         cubo[4].dot[4][1].riservato = 1;
         
         cubo[4].dot[4][2].x = 0.8;
         cubo[4].dot[4][2].z = 0.15;
         cubo[4].dot[4][2].y = -1;
         cubo[4].dot[4][2].riservato = 1;
         
         cubo[4].dot[4][3].x = 0.5;
         cubo[4].dot[4][3].z = 0.15;
         cubo[4].dot[4][3].y = -1;
         cubo[4].dot[4][3].riservato = 1;
         
         cubo[4].dot_visibile[4] = 0;
         
         // PUNTO 5
         cubo[4].dot[5][0].x = -0.8;
         cubo[4].dot[5][0].z = 0.5;
         cubo[4].dot[5][0].y = -1;
         cubo[4].dot[5][0].riservato = 1;
         
         cubo[4].dot[5][1].x = -0.5;
         cubo[4].dot[5][1].z = 0.5;
         cubo[4].dot[5][1].y = -1;
         cubo[4].dot[5][1].riservato = 1;
         
         cubo[4].dot[5][2].x = -0.5;
         cubo[4].dot[5][2].z = 0.8;
         cubo[4].dot[5][2].y = -1;
         cubo[4].dot[5][2].riservato = 1;
         
         cubo[4].dot[5][3].x = -0.8;
         cubo[4].dot[5][3].z = 0.8;
         cubo[4].dot[5][3].y = -1;
         cubo[4].dot[5][3].riservato = 1;
         
         cubo[4].dot_visibile[5] = 0;
         
         // PUNTO 6
         cubo[4].dot[6][0].x = 0.5;
         cubo[4].dot[6][0].z = 0.5;
         cubo[4].dot[6][0].y = -1;
         cubo[4].dot[6][0].riservato = 1;
         
         cubo[4].dot[6][1].x = 0.8;
         cubo[4].dot[6][1].z = 0.5;
         cubo[4].dot[6][1].y = -1;
         cubo[4].dot[6][1].riservato = 1;
         
         cubo[4].dot[6][2].x = 0.8;
         cubo[4].dot[6][2].z = 0.8;
         cubo[4].dot[6][2].y = -1;
         cubo[4].dot[6][2].riservato = 1;
         
         cubo[4].dot[6][3].x = 0.5;
         cubo[4].dot[6][3].z = 0.8;
         cubo[4].dot[6][3].y = -1;
         cubo[4].dot[6][3].riservato = 1;
         
         cubo[4].dot_visibile[6] = 1;
    
    // FACCIA 5 
    // PUNTO E
    cubo[5].vertice[0].x = 1;
    cubo[5].vertice[0].y = -1;
    cubo[5].vertice[0].z = 1;
    cubo[5].vertice[0].riservato = 1;
    
    // PUNTO F
    cubo[5].vertice[1].x = 1;
    cubo[5].vertice[1].y = 1;
    cubo[5].vertice[1].z = 1;
    cubo[5].vertice[1].riservato = 1; 
    
    // PUNTO G
    cubo[5].vertice[2].x = -1;
    cubo[5].vertice[2].y = 1;
    cubo[5].vertice[2].z = 1;
    cubo[5].vertice[2].riservato = 1; 
    
    // PUNTO H
    cubo[5].vertice[3].x = -1;
    cubo[5].vertice[3].y = -1;
    cubo[5].vertice[3].z = 1;
    cubo[5].vertice[3].riservato = 1;     
    
    cubo[5].visibile = 1;
    
    // SCORE PER FACCIA 5
         
         // PUNTO 0
         cubo[5].dot[0][0].x = -0.8;
         cubo[5].dot[0][0].y = -0.8;
         cubo[5].dot[0][0].z = 1;
         cubo[5].dot[0][0].riservato = 1;
         
         cubo[5].dot[0][1].x = -0.5;
         cubo[5].dot[0][1].y = -0.8;
         cubo[5].dot[0][1].z = 1;
         cubo[5].dot[0][1].riservato = 1;
         
         cubo[5].dot[0][2].x = -0.5;
         cubo[5].dot[0][2].y = -0.5;
         cubo[5].dot[0][2].z = 1;
         cubo[5].dot[0][2].riservato = 1;
         
         cubo[5].dot[0][3].x = -0.8;
         cubo[5].dot[0][3].y = -0.5;
         cubo[5].dot[0][3].z = 1;
         cubo[5].dot[0][3].riservato = 1;
         
         cubo[5].dot_visibile[0] = 1;
         
         // PUNTO 1
         cubo[5].dot[1][0].x = 0.5;
         cubo[5].dot[1][0].y = -0.8;
         cubo[5].dot[1][0].z = 1;
         cubo[5].dot[1][0].riservato = 1;
         
         cubo[5].dot[1][1].x = 0.8;
         cubo[5].dot[1][1].y = -0.8;
         cubo[5].dot[1][1].z = 1;
         cubo[5].dot[1][1].riservato = 1;
         
         cubo[5].dot[1][2].x = 0.8;
         cubo[5].dot[1][2].y = -0.5;
         cubo[5].dot[1][2].z = 1;
         cubo[5].dot[1][2].riservato = 1;
         
         cubo[5].dot[1][3].x = 0.5;
         cubo[5].dot[1][3].y = -0.5;
         cubo[5].dot[1][3].z = 1;
         cubo[5].dot[1][3].riservato = 1;
         
         cubo[5].dot_visibile[1] = 1;
         
         // PUNTO 2
         cubo[5].dot[2][0].x = -0.8;
         cubo[5].dot[2][0].y = -0.15;
         cubo[5].dot[2][0].z = 1;
         cubo[5].dot[2][0].riservato = 1;
         
         cubo[5].dot[2][1].x = -0.5;
         cubo[5].dot[2][1].y = -0.15;
         cubo[5].dot[2][1].z = 1;
         cubo[5].dot[2][1].riservato = 1;
         
         cubo[5].dot[2][2].x = -0.5;
         cubo[5].dot[2][2].y = 0.15;
         cubo[5].dot[2][2].z = 1;
         cubo[5].dot[2][2].riservato = 1;
         
         cubo[5].dot[2][3].x = -0.8;
         cubo[5].dot[2][3].y = 0.15;
         cubo[5].dot[2][3].z = 1;
         cubo[5].dot[2][3].riservato = 1;
         
         cubo[5].dot_visibile[2] = 1;
         
         // PUNTO 3
         cubo[5].dot[3][0].x = -0.15;
         cubo[5].dot[3][0].y = -0.15;
         cubo[5].dot[3][0].z = 1;
         cubo[5].dot[3][0].riservato = 1;
         
         cubo[5].dot[3][1].x = 0.15;
         cubo[5].dot[3][1].y = -0.15;
         cubo[5].dot[3][1].z = 1;
         cubo[5].dot[3][1].riservato = 1;
         
         cubo[5].dot[3][2].x = 0.15;
         cubo[5].dot[3][2].y = 0.15;
         cubo[5].dot[3][2].z = 1;
         cubo[5].dot[3][2].riservato = 1;
         
         cubo[5].dot[3][3].x = -0.15;
         cubo[5].dot[3][3].y = 0.15;
         cubo[5].dot[3][3].z = 1;
         cubo[5].dot[3][3].riservato = 1;
         
         cubo[5].dot_visibile[3] = 0;
         
         // PUNTO 4
         cubo[5].dot[4][0].x = 0.5;
         cubo[5].dot[4][0].y = -0.15;
         cubo[5].dot[4][0].z = 1;
         cubo[5].dot[4][0].riservato = 1;
    
         cubo[5].dot[4][1].x = 0.8;
         cubo[5].dot[4][1].y = -0.15;
         cubo[5].dot[4][1].z = 1;
         cubo[5].dot[4][1].riservato = 1;
         
         cubo[5].dot[4][2].x = 0.8;
         cubo[5].dot[4][2].y = 0.15;
         cubo[5].dot[4][2].z = 1;
         cubo[5].dot[4][2].riservato = 1;
         
         cubo[5].dot[4][3].x = 0.5;
         cubo[5].dot[4][3].y = 0.15;
         cubo[5].dot[4][3].z = 1;
         cubo[5].dot[4][3].riservato = 1;
         
         cubo[5].dot_visibile[4] = 1;
         
         // PUNTO 5
         cubo[5].dot[5][0].x = -0.8;
         cubo[5].dot[5][0].y = 0.5;
         cubo[5].dot[5][0].z = 1;
         cubo[5].dot[5][0].riservato = 1;
         
         cubo[5].dot[5][1].x = -0.5;
         cubo[5].dot[5][1].y = 0.5;
         cubo[5].dot[5][1].z = 1;
         cubo[5].dot[5][1].riservato = 1;
         
         cubo[5].dot[5][2].x = -0.5;
         cubo[5].dot[5][2].y = 0.8;
         cubo[5].dot[5][2].z = 1;
         cubo[5].dot[5][2].riservato = 1;
         
         cubo[5].dot[5][3].x = -0.8;
         cubo[5].dot[5][3].y = 0.8;
         cubo[5].dot[5][3].z = 1;
         cubo[5].dot[5][3].riservato = 1;
         
         cubo[5].dot_visibile[5] = 1;
         
         // PUNTO 6
         cubo[5].dot[6][0].x = 0.5;
         cubo[5].dot[6][0].y = 0.5;
         cubo[5].dot[6][0].z = 1;
         cubo[5].dot[6][0].riservato = 1;
         
         cubo[5].dot[6][1].x = 0.8;
         cubo[5].dot[6][1].y = 0.5;
         cubo[5].dot[6][1].z = 1;
         cubo[5].dot[6][1].riservato = 1;
         
         cubo[5].dot[6][2].x = 0.8;
         cubo[5].dot[6][2].y = 0.8;
         cubo[5].dot[6][2].z = 1;
         cubo[5].dot[6][2].riservato = 1;
         
         cubo[5].dot[6][3].x = 0.5;
         cubo[5].dot[6][3].y = 0.8;
         cubo[5].dot[6][3].z = 1;
         cubo[5].dot[6][3].riservato = 1;
         
         cubo[5].dot_visibile[6] = 1;
    
    
    punto_vista.x = -2;
    punto_vista.y = 2;
    punto_vista.z = 2;
    
     
    
    conversionePuntoDiVista( &puntoVistaS, &punto_vista );
    
    initwindow( 800, 600, ".: M-D3d :." );
    
    
    setcolor( 15 );
    DisegnaCubo( cubo, &puntoVistaS );
    
    while( 1 )
    {
       if ( ismouseclick(WM_LBUTTONDOWN))
       {
          do
          {
            MovimentoMouseCubo( mousex(), mousey() );
          } while ( !ismouseclick(WM_LBUTTONUP) );          
          clearmouseclick(WM_LBUTTONUP);
          clearmouseclick(WM_LBUTTONDOWN);               
       }
       else if ( ismouseclick(WM_RBUTTONDOWN))
       {
            for(int n=0 ; n<200 ; n++)
            {
                    setcolor(0);
                    DisegnaCubo( cubo, &puntoVistaS );
                    addVal = 0.01 * round(10);
                    puntoVistaS.teta += addVal;
                    addVal = 0.01 * round(10);
                    puntoVistaS.fi += addVal;
                    setcolor(15);
                    DisegnaCubo( cubo, &puntoVistaS );
                    delay(2);
            }
            clearmouseclick(WM_RBUTTONDOWN); 
       }
    }
           
    getch();
    
    system( "pause" );
    
    return 0;
}

//--------------------------------------------------------------------


void conversionePuntoDiVista( PUNTOSFERICO *a, PUNTO3D *b )
{
     a->ro = sqrt( pow( b->x, 2 ) + pow( b->y, 2 ) + pow( b->z, 2 ) ); 
     a->fi = atan( b->y / b->x );
     a->teta = acos( b->z / a->ro );  
}

void conversionePuntoSfericoaPuntoCartesiano( PUNTO3D *a, PUNTOSFERICO *b )
{
     a->x = b->ro * sin( b->fi ) * cos( b->teta );
     a->y = b->ro * sin( b->fi ) * sin( b->teta );
     a->z = b->ro * cos( b->fi );
     a->riservato = 1;
}

void trasformazioneNelleCordinateDiVista( PUNTOSFERICO *punto_vista, PUNTO3D *cordinate_vista, PUNTO3D *cordinate_poligono, int size )
{
     int i;
     double v[4][4];
     
     // Matrice per trasformare le cordinate visive
     v[0][0] = -sin( punto_vista->teta );
     v[0][1] = -cos( punto_vista->fi ) * cos( punto_vista->teta );
     v[0][2] = -sin( punto_vista->fi ) * cos( punto_vista->teta );
     v[0][3] = 0;
     
     v[1][0] = cos( punto_vista->teta );
     v[1][1] = -cos( punto_vista->fi ) * sin( punto_vista->teta );
     v[1][2] = -sin( punto_vista->fi ) * sin( punto_vista->teta );
     v[1][3] = 0;
     
     v[2][0] = 0;
     v[2][1] = sin( punto_vista->fi );
     v[2][2] = -cos( punto_vista->fi );
     v[2][3] = 0;
     
     v[3][0] = 0;
     v[3][1] = 0;
     v[3][2] = punto_vista->ro;
     v[3][3] = 1;
     
     for ( i = 0; i < size; i++ )
     {
         cordinate_vista[i].x = ( cordinate_poligono[i].x )*v[0][0] + ( cordinate_poligono[i].y )*v[1][0] + ( cordinate_poligono[i].z )*v[2][0] + ( cordinate_poligono[i].riservato )*v[3][0];    
         cordinate_vista[i].y = ( cordinate_poligono[i].x )*v[0][1] + ( cordinate_poligono[i].y )*v[1][1] + ( cordinate_poligono[i].z )*v[2][1] + ( cordinate_poligono[i].riservato )*v[3][1];
         cordinate_vista[i].z = ( cordinate_poligono[i].x )*v[0][2] + ( cordinate_poligono[i].y )*v[1][2] + ( cordinate_poligono[i].z )*v[2][2] + ( cordinate_poligono[i].riservato )*v[3][2];
         cordinate_vista[i].riservato = ( cordinate_poligono[i].x )*v[0][3] + ( cordinate_poligono[i].y )*v[1][3] + ( cordinate_poligono[i].z )*v[2][3] + ( cordinate_poligono[i].riservato )*v[3][3];
     }
}

void trasformazioneFacceCordinateDiVista( PUNTOSFERICO *punto_vista, POLIGONO3D *poligono_vista , POLIGONO3D *poligono, int size )
{
     int i, j;
     
     for ( i = 0; i < size; i++ )
     {
         trasformazioneNelleCordinateDiVista( punto_vista, poligono_vista[i].vertice, poligono[i].vertice, VERTICI );     
         poligono_vista[i].visibile = poligono[i].visibile; 
     }
     
     for ( j = 0; j < size; j++ )
         for ( i = 0; i < PUNTI; i++ )
         {
             trasformazioneNelleCordinateDiVista( punto_vista, &( poligono_vista[j].dot[i][0] ), &( poligono[j].dot[i][0] ), VERTICI ); 
             poligono_vista[j].dot_visibile[i] = poligono[j].dot_visibile[i];
         }    
}

void trasformazioneNelleCordinateDueD( double d, PUNTO2D *cordinate_2d, PUNTO3D *cordinate_vista, int size )
{
     int i;
     double x_c, y_c;
     
     x_c = getmaxx() / 2;
     y_c = getmaxy() / 2;
     
     
     for ( i = 0; i < size; i++ )
     {
         cordinate_2d[i].x = d * ( cordinate_vista[i].x / cordinate_vista[i].z ) + x_c;
         cordinate_2d[i].y = d * ( cordinate_vista[i].y / cordinate_vista[i].z ) + y_c;              
     } 
}

void trasformazioniFacceCordinateDueD( double d, POLIGONO2D *poligono_2d, POLIGONO3D *poligono_vista, int size )
{
     int i, j;
     
     for ( i = 0; i < size; i++ )
     {
         trasformazioneNelleCordinateDueD( d, poligono_2d[i].punto, poligono_vista[i].vertice, VERTICI );     
         poligono_2d[i].visibile = poligono_vista[i].visibile;
     } 
     
      for ( j = 0; j < size; j++ )
         for ( i = 0; i < PUNTI; i++ )
         {
             trasformazioneNelleCordinateDueD( d, &( poligono_2d[j].dot[i][0] ), &( poligono_vista[j].dot[i][0] ), VERTICI );
             poligono_2d[j].dot_visibile[i] = poligono_vista[j].dot_visibile[i];
         } 
     
}

double diemnsioneMaxOggetto( POLIGONO3D *cordinate_poligono )
{
       return 6;
}

double maxSchermo()
{
       if ( getmaxx() >= getmaxy() )
          return ( double )getmaxx();
       else
          return ( double )getmaxy();        
}

double distanzaCamera( PUNTOSFERICO *punto_vista, POLIGONO3D *cordinate_poligono  )
{
       return punto_vista->ro*( ( maxSchermo() - 10 ) / diemnsioneMaxOggetto( cordinate_poligono ) ); 
}

int arrotonda( double a )
{
    if ( a - floor( a ) >= 0.5 )
       return ceil( a );
    else
        return floor( a );   
}

void disegnaFaccia( PUNTO2D *punto, int size )
{
     int i;
     
     //circle( arrotonda( ( punto[0].x + punto[2].x ) / 2  ), arrotonda( ( punto[0].y + punto[2].y ) / 2 ), 3 );
      
     for ( i = 0; i < size - 1; i++ )
         line( arrotonda( punto[i].x ), arrotonda( punto[i].y ), arrotonda( punto[i+1].x ), arrotonda( punto[i+1].y ) );
     
     line( arrotonda( punto[size-1].x ), arrotonda( punto[size-1].y ), arrotonda( punto[0].x ), arrotonda( punto[0].y ) );
}

void disegnaSolido( POLIGONO2D *poligono, int size )
{
     int i, j;
     
     for ( i = 0; i < size; i++ )
         if ( poligono[i].visibile == 1 )
         {
           disegnaFaccia( poligono[i].punto, VERTICI );
         
           for ( j = 0; j < PUNTI; j++ )
                if ( poligono[i].dot_visibile[j] == 1 )
                  disegnaFaccia( &( poligono[i].dot[j][0] ), VERTICI );
           
         }      
}

void DisegnaCubo( POLIGONO3D *cubo, PUNTOSFERICO *puntoVistaS )
{
     int i, j, n = 6;
     POLIGONO3D vista[FACCE];
     POLIGONO2D cubo2d[FACCE];
     PUNTO3D punto_vista;
     
     conversionePuntoSfericoaPuntoCartesiano( &punto_vista, puntoVistaS ); 
           
     for ( i = 0; i < FACCE; i++ )
     {
        //printf( "\n------>FACCIA %d<--------\n", i );
        cubo[i].visibile = CalcoloFacciaNascota( &punto_vista, cubo[i].vertice, VERTICI );
     }
     
     trasformazioneFacceCordinateDiVista( puntoVistaS, vista, cubo, FACCE );
     trasformazioniFacceCordinateDueD( distanzaCamera( puntoVistaS, cubo ), cubo2d, vista, FACCE );
     disegnaSolido( cubo2d, FACCE );         
}

int VerticePiuSinistra( PUNTO3D *vertice, int size )
{
     double temp;
     int i, j;
     
     temp = vertice[0].x;
     j = 0;
     
     for ( i = 1; i < size; i++ )
         if ( vertice[i].x > temp )
         {
            temp = vertice[i].x;
            j = i;
         }
            
     return j; 
}

int CalcoloFacciaNascota( PUNTO3D *punto_vista, PUNTO3D *vertice, int size )
{
    int l, lb, la;
    double dir;
    PUNTO3D s, v1, v2, r;
    
    l = 0;//= VerticePiuSinistra( vertice, size );
    s.x = punto_vista->x - vertice[l].x;
    s.y = punto_vista->y - vertice[l].y;
    s.z = punto_vista->z - vertice[l].z;
    s.riservato = 1;   
    
    if ( l == 0 )
       lb = size - 1;
    else
        lb = l - 1;
    
    v1.x =  vertice[lb].x - vertice[l].x;
    v1.y =  vertice[lb].y - vertice[l].y;
    v1.z =  vertice[lb].z - vertice[l].z;
    
    
    if ( l == size - 1 )
       la = 0;
    else
        la = l + 1;
        
    v2.x = vertice[la].x - vertice[l].x;
    v2.y = vertice[la].y - vertice[l].y;
    v2.z = vertice[la].z - vertice[l].z;
    
    r.x = v1.y * v2.z - v1.z * v2.y;
    r.y = v1.z * v2.x - v1.x * v2.z;
    r.z = v1.x * v2.y - v1.y * v2.x;
          
    dir = r.x * s.x + r.y * s.y + r.z * s.z;
    
    /*
    printf ( "PUNTO DI VISTA s[x, y, z] = [%.3f, %.3f, %.3f]\n", s.x, s.y, s.z ); 
    printf ( "P v1[x, y, z] = [%.3f, %.3f, %.3f]\n", v1.x, v1.y, v1.z );
    printf ( "Q v2[x, y, z] = [%.3f, %.3f, %.3f]\n", v2.x, v2.y, v2.z );
    printf ( "Risultante r[x, y, z] = [%.3f, %.3f, %.3f]\n", r.x, r.y, r.z );
    printf( "Direzione dir = %.3f\n", dir );
    */
    
    if ( dir < 0 )
       return 1;
    else
        return 0;
}

void MovimentoMouseCubo( int x, int y )
{
     static int v_x, v_y;
     
     if ( v_x != x || v_y != y )
     { 
        setcolor( 0 );
        DisegnaCubo( cubo, &puntoVistaS );
     
        if ( v_x != x )
        {
           if ( x > v_x )
           {
              puntoVistaS.teta += 0.03;
              v_x = x;
           }    
           else
           {
               puntoVistaS.teta -= 0.03;
               v_x = x;
           }
        }   
     
        if ( v_y != y )
        {
           if ( y > v_y )
           {
              puntoVistaS.fi += 0.03;
              v_y = y;
           }    
           else
           {
               puntoVistaS.fi -= 0.03;
               v_y = y;
           }
        } 
     
        setcolor( 15 );
        DisegnaCubo( cubo, &puntoVistaS );
     }
}



